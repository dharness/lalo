angular.module('lalo.controllers', []);
var app = angular.module('lalo', ['ionic', 'lalo.controllers']);



app.run(function($ionicPlatform)
{
	$ionicPlatform.ready(function()
	{
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard)
		{
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
		}
		if (window.StatusBar)
		{
			StatusBar.styleDefault();
		}
	});
});

app.config(function($stateProvider, $urlRouterProvider)
{
	$urlRouterProvider.otherwise('/login');

	$stateProvider.state('login',
	{
		url: '/login',
		templateUrl: 'templates/login.html',
		controller: 'LoginController'
	})

	$stateProvider.state('register',
	{
		url: '/register',
		templateUrl: 'templates/register.html',
		controller: 'RegisterController'
	})

});