angular.module('lalo-auth', [])


.factory('Auth', function($rootScope, $http, $q, Session, AUTH_EVENTS)
{
	var user;

	return {
		login: function(options)
		{
			return $q(function(resolve, reject)
			{
				if (options.username && options.password)
				{
					var user = {};
					Session.create('token', 'username')
					resolve(user);
				}
				else
				{
					reject('Error')
				}
			})

		},
		remoteLogin: function(options)
		{
			var body = {
				"password": options.password,
				"username": options.username,
				"grant_type": "password",
				"scope": "read"
			};

			var req = {
				method: 'POST',
				url: 'http://aws.dvornikov.ca:8080/backend/oauth/token',
				headers:
				{
					'Accept': 'application/json',
					"Authorization": "Basic Y2xpZW50LWlvczoxMjM0NTY=",
					"Content-Type": " application/x-www-form-urlencoded"
				},
				data: "password=password&username=test@test.ca&grant_type=password&scope=read"
			};

			$http(req).success(function(data)
			{
				console.log(data);
				$('#output').html(data)
			}).error(function(err)
			{
				console.log(err);
			});
		},
		isLoggedIn: function()
		{
			return !!Session.username;
		}
	}

});