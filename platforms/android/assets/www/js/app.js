angular.module('lalo.controllers', []);
var app = angular.module('lalo', ['ionic', 'lalo.controllers']);

$.ajax(
{
	type: "POST",
	url: "http://aws.dvornikov.ca:8080/backend/oauth/token",
	data:
	{
		"password": "password",
		"username": "test@test.ca",
		"grant_type": "password",
		"scope": "read"
	},
	contentType: "application/x-www-form-urlencoded",
	dataType: "jsonp",
	beforeSend: function(xhr)
	{
		xhr.setRequestHeader("Authorization", "Basic Y2xpZW50LWlvczoxMjM0NTY=");
		xhr.setRequestHeader("Accept", "application/json");
	},
	success: function()
	{
		alert('SUCESS');
	},
	error: function(err)
	{
		console.log(err);
	}
});

app.run(function($ionicPlatform)
{
	$ionicPlatform.ready(function()
	{
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard)
		{
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
		}
		if (window.StatusBar)
		{
			StatusBar.styleDefault();
		}
	});
});

app.config(function($stateProvider, $urlRouterProvider)
{
	$urlRouterProvider.otherwise('/login');

	$stateProvider.state('login',
	{
		url: '/login',
		templateUrl: 'templates/login.html',
		controller: 'LoginController'
	})

	$stateProvider.state('register',
	{
		url: '/register',
		templateUrl: 'templates/register.html',
		controller: 'RegisterController'
	})

});