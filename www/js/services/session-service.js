angular.module('lalo-session', [])

.service('Session', function()
{

	this.create = function(token, username)
	{
		this.token = token;
		this.username = username;
	};

	this.destroy = function()
	{
		this.token = null;
		this.username = null;
	};

});