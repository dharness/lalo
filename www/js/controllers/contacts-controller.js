angular.module('lalo.controllers').controller('ContactsController', ['$scope',
	'$http', '$location', '$rootScope', 'Auth', 'AUTH_EVENTS',

	function($scope, $http, $location, $rootScope, Auth, AUTH_EVENTS)
	{

		$scope.contacts = [
		{
			name: "Jim Smith",
			message: "I'm just your average.. Jim",
			img: "img/girl.png"
		},
		{
			name: "Jill Franco",
			message: "Wednesday, shirts and anything in the spleen",
			img: "img/girl.png"
		},
		{
			name: "Rollandson Wet",
			message: "Soggy bottom moist wet everything upside down",
			img: "img/girl.png"
		},
		{
			name: "Mendle Birt",
			message: "Why are you even reading these things",
			img: "img/girl.png"
		},
		{
			name: "Tagitha Woe",
			message: "Tagitha, hag of a century, constantly stricken with sick and morose",
			img: "img/girl.png"
		},
		{
			name: "Po Woe",
			message: "Not much to say abodut myself really from the perspective of an eel",
			img: "img/girl.png"
		}];
		$scope.hideSearch = true;

	}
]);