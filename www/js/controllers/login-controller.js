angular.module('lalo.controllers').controller('LoginController', ['$scope',
	'$http', '$location', '$rootScope', 'Auth', 'AUTH_EVENTS',

	function($scope, $http, $location, $rootScope, Auth, AUTH_EVENTS)
	{
		console.log(Auth);
		$scope.data = {};
		$scope.login = function()
		{

			Auth.login(
			{
				username: $scope.data.username,
				password: $scope.data.password
			}).then(function()
			{
				$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
				$location.path('/register');
			}, function()
			{
				$rootScope.$broadcast(AUTH_EVENTS.loginFailed);
				alert('Loggin failed')
			});


		}
	}
]);