angular.module('lalo.controllers')

.controller('ChatController', ['$scope',

	function($scope)
	{
		$scope.data = {};

		$scope.messages = [
		{
			text: "lemmons",
			id: "me"
		},
		{
			text: "dd",
			id: "not me"
		}];

		$scope.sendMessage = function()
		{
			$scope.data.message && $scope.messages.push(
			{
				text: $scope.data.message,
				id: "me"
			});

			$scope.data.message = "";
		}
	}
])