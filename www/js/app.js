angular.module('lalo.controllers', []);
var app = angular.module('lalo', ['ionic', 'lalo-auth', 'lalo-session',
	'lalo.controllers'
]);

app.run(function($ionicPlatform, $rootScope, $location, Auth)
{
	// BASIC IONIC SETUP
	$ionicPlatform.ready(function()
	{
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard)
		{
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
		}
		if (window.StatusBar)
		{
			StatusBar.styleDefault();
		}
	});

	// AUTHENTICATION
	$rootScope.$on('$stateChangeStart', function(event, next)
	{
		console.log();
		if (!Auth.isLoggedIn() && next.url !== "/login")
		{
			console.log(next);
			// event.preventDefault();
			// $location.path('/login');
		}
	});

});

app.constant('AUTH_EVENTS',
{
	loginSuccess: 'auth-login-success',
	loginFailed: 'auth-login-failed',
	logoutSuccess: 'auth-logout-success',
	sessionTimeout: 'auth-session-timeout',
	notAuthenticated: 'auth-not-authenticated',
	notAuthorized: 'auth-not-authorized'
})

app.config(function($stateProvider, $urlRouterProvider)
{
	$urlRouterProvider.otherwise('/chat');

	$stateProvider.state('login',
	{
		url: '/login',
		templateUrl: 'templates/login.html',
		controller: 'LoginController'
	})

	$stateProvider.state('register',
	{
		url: '/register',
		templateUrl: 'templates/register.html',
		controller: 'RegisterController'
	})

	$stateProvider.state('contacts',
	{
		url: '/contacts',
		templateUrl: 'templates/contacts.html',
		controller: 'ContactsController'
	})

	$stateProvider.state('chat',
	{
		url: '/chat',
		templateUrl: 'templates/chat.html',
		controller: 'ChatController'
	})

	$stateProvider.state('profile',
	{
		url: '/profile',
		templateUrl: 'templates/profile.html',
		controller: 'ProfileController'
	})
});